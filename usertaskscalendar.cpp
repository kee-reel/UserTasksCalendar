#include "usertaskscalendar.h"

UserTasksCalendar::UserTasksCalendar(QObject* parent, ReferenceInstancePtr<IUserTaskDateDataExtention> userTasks) :
	QObject(parent),
	m_userTasks(userTasks)
{
}

void UserTasksCalendar::init()
{
	connect(m_userTasks.reference()->object(), SIGNAL(modelChanged()), SLOT(updateModel()));
	auto model = m_userTasks->getModel();
	if(!model.isNull())
		updateModel();
}

QObject* UserTasksCalendar::getTasksForDay(QDate day)
{
	auto container = m_dayToTasks.find(day);
	if(container != m_dayToTasks.end())
		return container.value();
	else
		return m_dayToTasks[day] = new DayTaskContainer(this);
}

void UserTasksCalendar::updateModel()
{
	auto model = m_userTasks->getModel();
	connect(model.data(), SIGNAL(modelChanged()), SLOT(updateCalendarData()));
	updateCalendarData();
}

void UserTasksCalendar::updateCalendarData()
{
	m_dayToTasks.clear();
	auto model = m_userTasks->getModel();
	auto itemIds = model->getItemIds();

	for(auto& container : m_dayToTasks)
	{
		container->clearTasks();
	}

	for(auto& id : itemIds)
	{
		auto data = model->getItem(id);
		auto name = data[INTERFACE(IUserTaskDataExtention)]["name"].toString();
		auto startDate = data[INTERFACE(IUserTaskDateDataExtention)]["startDate"].toDateTime();
		auto dueDate = data[INTERFACE(IUserTaskDateDataExtention)]["dueDate"].toDateTime();
		auto& container = m_dayToTasks[startDate.date()];
		if(!container)
			container = new DayTaskContainer(this);
		container->appendTask(name, startDate, dueDate);
	}

	for(auto& container : m_dayToTasks)
	{
		container->notify();
	}
}
